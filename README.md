# Minecraft Java Edition installer for Linux





This project will install the ely.by launcher so you can play Minecraft Java Edition without any hassle.
No matter the distro, this installer is universal

### Features
* Universal installer, it doesn't matter if your PC is ARM or x86, 32-bit or 64-bit.
* Java does not need to be installed on your system, this installer has its own Java


## Installation

Download [setup.sh](https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/setup.sh?inline=false)


---

Remember to allow execution

```
chmod +x setup.sh
```
---

Execute

```
./setup.sh
```
# Possible problems

### **If you can't install**

* Before trying again, clear the installer cache:
```
rm -Rf /tmp/arminho-cache
```

###

* Make sure you have unzip and zenity installed:


For Debian/Ubuntu and derivates
```
sudo apt install unzip zenity
```

For Arch Linux/Manjaro and others
```
sudo pacman -S unzip zenity
```

For Fedora
```
sudo dnf install unzip zenity
```

--- 
