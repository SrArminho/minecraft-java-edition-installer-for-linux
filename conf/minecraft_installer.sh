#!/usr/bin/env bash

cache="/tmp/arminho-cache"
install=`cat $cache/user/minecraft/install`
arch=`cat $cache/user/minecraft/arch`
dir=`cat $cache/user/minecraft/dir`
shortcut=`cat $cache/user/minecraft/shortcut`


if [ $install = "1" ]
then
    wget -P $cache/downloads https://ely.by/load/launcher/dl/mcl/jar
    mkdir $cache/downloads/Minecraft
    mkdir $cache/downloads/Minecraft/images
    mv $cache/downloads/jar $cache/downloads/Minecraft/TL_mcl.jar
    wget -P $cache/downloads/Minecraft https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/conf/AppRun.sh
    chmod +x $cache/downloads/Minecraft/AppRun.sh
    wget -P $cache/downloads/Minecraft/images https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/images/icon.png
    wget -P $cache/downloads/Minecraft/images https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/images/horizontal_poster.png
    wget -P $cache/downloads/Minecraft/images https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/images/vertical_poster.jpg
    wget -P $cache/downloads https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/conf/model.desktop

    if [ $arch = "x86_64" ]
    then
        wget -P $cache/downloads https://github.com/alinke/pixelcade-jre/raw/main/jdk-x86-64.zip
        unzip $cache/downloads/jdk-x86-64.zip -d $cache/downloads/Minecraft
        mv $cache/downloads/Minecraft "$dir/Minecraft"
    fi
    if [ $arch = "x86_32" ]
    then
        wget -P $cache/downloads https://github.com/alinke/pixelcade-jre/raw/main/jdk-x86-32.zip
        unzip $cache/downloads/jdk-x86-32.zip -d $cache/downloads/Minecraft
        mv $cache/downloads/Minecraft "$dir/Minecraft"
    fi
    if [ $arch = "aarch64" ]
    then
        wget -P $cache/downloads https://github.com/alinke/pixelcade-jre/raw/main/jdk-aarch64.zip
        unzip $cache/downloads/jdk-aarch64.zip -d $cache/downloads/Minecraft
        mv $cache/downloads/Minecraft "$dir/Minecraft"
    fi
    if [ $arch = "aarch32" ]
    then
        wget -P $cache/downloads https://github.com/alinke/pixelcade-jre/raw/main/jdk-aarch32.zip
        unzip $cache/downloads/jdk-aarch32.zip -d $cache/downloads/Minecraft
        mv $cache/downloads/Minecraft "$dir/Minecraft"
    fi
    if [ $shortcut = "1" ]
    then
        sed -i "s:executable:$dir/Minecraft/AppRun.sh:" $cache/downloads/model.desktop
        sed -i "s:dir:$dir/Minecraft:" $cache/downloads/model.desktop
        sed -i "s:micon:$dir/Minecraft/images/icon.png:" $cache/downloads/model.desktop
        mv $cache/downloads/model.desktop $HOME/.local/share/applications/Minecraft.desktop
    fi  
fi
