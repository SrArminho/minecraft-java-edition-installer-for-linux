#!/usr/bin/env bash

##Creating cache files
mkdir /tmp/arminho-cache
cache="/tmp/arminho-cache"
	#Security clean-up
rm -Rf $cache/user/*
rm -Rf $cache/downloads/*
	#Main folders
mkdir $cache/downloads
mkdir $cache/user
mkdir $cache/user/minecraft
mkdir $cache/scripts

clear


    #Setup
echo -e "https://gitlab.com/SrArminho\n\nWelcome to  installer of Ely.by launcher for Minecraft (unnoficial)\n\n"

zenity --title "Select the installation folder" --file-selection --directory > $cache/user/minecraft/dir


    #desktop file
echo -e "\n\nCreate shortcut in apps menu?\n\n1 - Yes\n0 - No"
read shortcut && echo "$shortcut" > $cache/user/minecraft/shortcut 
while [ $shortcut != "1" -a $shortcut != "0" ]
do
    echo 'Plase type again'
    read shortcut && echo "$shortcut" > $cache/user/minecraft/shortcut
done


    #Proceed to installation

echo -e "\n\nProceed to installation?\n\n1 - Yes\n0 - No"
read install && echo "$install" > $cache/user/minecraft/install

while [ $install != "1" -a $install != "0" ]
do
    echo 'Plase type again'
    read install && echo "$install" > $cache/user/minecraft/install
done

uname -m > $cache/user/minecraft/arch

wget -P $cache/scripts https://gitlab.com/SrArminho/minecraft-java-edition-installer-for-linux/-/raw/main/conf/minecraft_installer.sh
chmod +x $cache/scripts/minecraft_installer.sh
$cache/scripts/minecraft_installer.sh

clear

echo "All done!"
read
